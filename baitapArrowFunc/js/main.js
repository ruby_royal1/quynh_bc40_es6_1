const colorList = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender", "celadon", "saffron", "fuschia", "cinnabar"];
let cont = document.getElementById("colorContainer");
let house = document.getElementById("house");
let cPick = document.getElementsByClassName("color-button");
var i = 0;
cont.innerHTML = "";
a = cont.innerHTML;
var loadColorList = (() => {
    for (let i = 0; i < colorList.length; i++) {
        if (i == 0) {
            a = "<button class='color-button " + colorList[i] + " active'></button>";
        } else {
            a = "<button class='color-button " + colorList[i] + "'></button>"
        }
        cont.innerHTML += a;
    }
});
loadColorList();

swapColor = ((i, a) => {
    for (let i = 0; i < cPick.length; i++) {
        cPick[i].classList.remove("active");
       
    }
    cPick[a].classList.add("active"),
    house.className = "house " + i;
});

for (let i = 0; i < cPick.length; i++) {
    cPick[i].addEventListener("click", function () {
        swapColor(colorList[i], i)
    });

}